package com.night.myapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.night.myapplication.databinding.ListLayoutBinding
import com.night.myapplication.model.Food

class FoodAdapter(private val onItemClick: (Food) -> Unit) :
    RecyclerView.Adapter<FoodItemListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodItemListViewHolder {
        return FoodItemListViewHolder(
            binding = ListLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            onItemClick = onItemClick
        )
    }

    override fun onBindViewHolder(holder: FoodItemListViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size

    fun setData(data : List<Food>){
        differ.submitList(data)
        notifyItemRangeChanged(0,data.size)

    }
    private val differ = AsyncListDiffer(this, object : DiffUtil.ItemCallback<Food>(){
        override fun areItemsTheSame(oldItem: Food, newItem: Food): Boolean {
            return oldItem.name == newItem.name  && oldItem.desc == newItem.desc && oldItem.desc == newItem.desc && oldItem.desc == newItem.desc && oldItem.desc == newItem.desc
        }

        override fun areContentsTheSame(oldItem: Food, newItem: Food): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }

    })


}


class FoodItemListViewHolder(
    private val binding: ListLayoutBinding,
    private val onItemClick: (Food) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(food: Food) {
        with(binding) {
            tvListName1.text = food.name
            tvListPrice1.text ="Rp " +  food.price.toString()
            ivListFood1.load(food.img)
        }
        binding.root.setOnClickListener{
            onItemClick.invoke(food)
        }
    }
}