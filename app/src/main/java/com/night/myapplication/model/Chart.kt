package com.night.myapplication.model

data class Chart(
    var menuName:String,
    var quantity : Int
)
