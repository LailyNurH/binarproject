package com.night.myapplication.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Food(
    var price:Double,
    var desc:String,
    var name:String,
    var img: Int ,

):Parcelable
