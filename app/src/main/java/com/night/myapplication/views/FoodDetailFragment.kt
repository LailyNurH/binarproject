package com.night.myapplication.views

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import coil.load
import com.night.myapplication.R
import com.night.myapplication.databinding.FragmentFoodDetailBinding
import com.night.myapplication.model.Food


class FoodDetailFragment : Fragment() {
    private lateinit var binding : FragmentFoodDetailBinding
    private var currentAmount = 1
    private val viewModel: MainViewModel by viewModels()

    private val food: Food by lazy {
        FoodDetailFragmentArgs.fromBundle(arguments as Bundle).food
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentFoodDetailBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        withViewModel()
        binding.apply {
            ivDetailFood.load(food.img)
            tvFoodDescription.text = food.desc
            tvFoodPrice.text = "Rp ${food.price}"
            foodName.text = food.name

            tvLocation.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://maps.app.goo.gl/h4wQKqaBuXzftGK77"))
                startActivity(intent)
            }

            setAmount()
        }

    }

    private fun withViewModel() {
        binding.apply {
            cvPlus.setOnClickListener {
                wIncrementCount()
            }
            cvMin.setOnClickListener {
                wDecrementCount()
            }
            viewModel.vCounter.observe(requireActivity()){result->
                binding.tvAmount.text= result.toString()
            }

        }
    }

    private fun wDecrementCount() {
        viewModel.decrementCount()
    }

    private fun wIncrementCount() {
        viewModel.incrementCount()
    }

    private fun setAmount() {

        binding.apply {
            tvTotalPrice.text = (currentAmount * food.price).toString()

            cvPlus.setOnClickListener {
                currentAmount++
                tvAmount.text = currentAmount.toString()
                tvTotalPrice.text = (currentAmount * food.price).toString()
            }
            cvMin.setOnClickListener {
                if (currentAmount > 0) {
                    currentAmount--
                    tvAmount.text = currentAmount.toString()
                    tvTotalPrice.text = (currentAmount * food.price).toString()

                }
            }
        }


    }


}