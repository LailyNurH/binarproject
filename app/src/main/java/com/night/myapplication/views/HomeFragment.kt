package com.night.myapplication.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.night.myapplication.R
import com.night.myapplication.adapter.FoodAdapter
import com.night.myapplication.adapter.GridFoodAdapter
import com.night.myapplication.databinding.FragmentHomeBinding
import com.night.myapplication.datasource.FoodDataSourceImpl
import com.night.myapplication.model.Food


class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding

    private val foodAdapter : FoodAdapter by lazy {
        FoodAdapter{
            toDetailFragment(it)

        }
    }

    private val gridAdapter : GridFoodAdapter by lazy {
        GridFoodAdapter{
            toDetailFragment(it)

        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentHomeBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            ivList.visibility = View.GONE

            rvFood.adapter = foodAdapter
            binding.rvFood.layoutManager = LinearLayoutManager(requireContext())
            foodAdapter.setData(FoodDataSourceImpl().getFood())

            ivList.setOnClickListener {
                ivList.visibility = View.GONE
                ivGrid.visibility = View.VISIBLE

                rvFood.adapter = foodAdapter
                binding.rvFood.layoutManager = LinearLayoutManager(requireContext())
                foodAdapter.setData(FoodDataSourceImpl().getFood())

            }

            ivGrid.setOnClickListener {
                ivList.visibility = View.VISIBLE
                ivGrid.visibility = View.GONE

                rvFood.adapter = gridAdapter
                binding.rvFood.layoutManager =  GridLayoutManager(requireContext(), 2)
                gridAdapter.setData(FoodDataSourceImpl().getFood())



            }


        }

    }


    private fun toDetailFragment(it: Food) {
        val action =HomeFragmentDirections.actionHomeFragmentToFoodDetailFragment(it)
        findNavController().navigate(action)

    }


}